#!/bin/sh
set -e

[ -d /dev/net ] ||
    mkdir -p /dev/net
[ -c /dev/net/tun ] ||
    mknod /dev/net/tun c 10 200

if [ "$1" = "bash" ]; then
    exec "$@"
    exit $?
fi

# allow changing debug mode
if [ -z "$PRITUNL_DEBUG" ]; then
    PRITUNL_DEBUG="false"
fi

# allow changing bind addr
if [ -z "$PRITUNL_BIND_ADDR" ]; then
    PRITUNL_BIND_ADDR="0.0.0.0"
fi

if [ -z "$PRITUNL_SERVER_CERT_PATH" ]; then
    PRITUNL_SERVER_CERT_PATH="/var/lib/pritunl/pritunl.crt"
fi
if [ -z "$PRITUNL_SERVER_KEY_PATH" ]; then
    PRITUNL_SERVER_KEY_PATH="/var/lib/pritunl/pritunl.key"
fi

if [ -z "$PRITUNL_MONGODB_URI" ]; then
    echo "Error **** Must provide value for `PRITUNL_MONGODB_URI`"
    exit 1
fi

cat << EOF > /etc/pritunl.conf
{
    "bind_addr": "$PRITUNL_BIND_ADDR",
    "debug": $PRITUNL_DEBUG,
    "local_address_interface": "auto",
    "log_path": "/var/log/pritunl.log",
    "mongodb_uri": "$PRITUNL_MONGODB_URI",
    "server_cert_path": "$PRITUNL_SERVER_CERT_PATH",
    "server_key_path": "$PRITUNL_SERVER_KEY_PATH",
    "static_cache": true,
    "temp_path": "/tmp/pritunl_%r",
    "www_path": "/usr/share/pritunl/www"
}
EOF

exec "$@"

